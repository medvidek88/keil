var express			= require('express');
var app 			= express();
var http			= require('http').createServer(app);
var fs 				= require('fs');
var querystring 	= require('querystring');
var bodyParser 		= require('body-parser');

var httpPort 		= process.env.HTTP_PORT || 8005;
var dataFile 		= process.env.DATA_FILE || "data.json";

app.use(bodyParser.urlencoded({extended: false})); 
app.use(bodyParser.json());
app.use("/assets", express.static(__dirname + '/assets'));
app.set('view engine', 'pug');

app.get('/', function(req, res){
	require('fs').readFile(dataFile, 'utf8', function (err, data) {
		if (err) {
			var dataFromFile = [];
		}
		else{
			var dataFromFile = JSON.parse(data);
		}
		res.render('index', { data: dataFromFile });
	});
});

app.post('/save', function(req, res){
	if(typeof req.body.name !== "undefined" && req.body.name !== ""){
		require('fs').readFile(dataFile, 'utf8', function (err, data) {
			if (err) {
				res.render('index', { data: [], errors: [{"msg" : "cannot read from file"}] });
			}
			else{
				var dataFromFile = JSON.parse(data);
				dataFromFile.push(req.body.name);
				fs.writeFile(dataFile, JSON.stringify(dataFromFile), function(err) {
					if(err) {
						res.render('index', { data: [], errors: [{"msg" : "cannot write to file"}] });
					}
					res.redirect('/');
				}); 
			}
		});
	}
	else{
		res.render('index', { data: [], errors: [{"msg" : "missing value for Name"}] });
	}
});

http.listen(httpPort, function(){
	console.log('HTTP LOG server started with port: ' + httpPort);
});
